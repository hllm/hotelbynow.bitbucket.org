Hola Bruno,

Por lo que puedo ver en el sistema, tu técnico ya ha sido contactado el 8 de Febrero. Ruego revises con él es status del desarrollo y para cualquier duda técnica se dirija a mi compañero Mateu a la dirección apitude@hotelbeds.com

8/02/2018 11:48 by Mateu Sora Quevedo Hide details 

[Sent: 8/02/2018 11:48]
[From: apitude@hotelbeds.com]
[To: ricardo@sierratecnoligia.com.br]
[Subject: #10457634 - API Accommodation - SNOW EVOLUTION [ ]]

Dear RICARDO SIERRA,

Thank you for your interest in our API Integration.  We believe you will find our APItude Developer Portal easy to use and the integration process will be simple and fast.

To get started, if you haven´t already, please:
1)     Register in our APItude Developer Portal (https://developer.hotelbeds.com/) and
2)     Create an Application to gain access to your API Key: Go to "My Account" and create an Application to gain access to your API Key (APITUDE EVALUATION PLAN) https://developer.hotelbeds.com/member/my-account.

This API Key will give you access to all documentation and basic test environment. 

https://developer.hotelbeds.com/io-docs

**Please note that we do not recommend for you to develop using APITUDE EVALUATION PLAN key. As it's for 'trial' with very limited access: 1call/sec, 50 calls per day.

Once you are ready to begin developing, please:

3)     Request access to our pre-production environment (https://developer.hotelbeds.com/contact) and we will upgrade your API Key to enable you to begin developing with us. 

If you have any questions or doubts along the way, Id be pleased to clarify them for you. 

Thanks.

 

Regards.

Maria José Martin
Sales  - Europe
Hotelbeds

tel: (+34) 971 624 732 - Ext 88732| mob: (+34) 638 065 386 | skype: emjeymartin
Ed. Mirall | Cami de Son Fangos 100, A-5
E-07007, Palma de Mallorca, Spain
www.hotelbeds.com

Hotelbeds